class UserModel {
  String id;
  String? photoUrl;
  String nickname;
  String? aboutMe;

  UserModel(
      {required this.id, this.photoUrl, required this.nickname, this.aboutMe});

  UserModel.fromJson(Map<String, Object?> json)
      : this(
          id: json['id'] as String,
          photoUrl:
              json['photoUrl'] != null ? json['photoUrl'] as String : null,
          nickname: json['nickname']! as String,
          aboutMe: json['aboutMe'] != null ? json['aboutMe']! as String : null,
        );

  Map<String, Object?> toJson() {
    return {'photoUrl': photoUrl, 'nickname': nickname, 'aboutMe': aboutMe};
  }
}
