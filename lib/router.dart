import 'package:flum_chat/bindings/setting_bingding.dart';
import 'package:flum_chat/ui/screens/chat.dart';
import 'package:flum_chat/ui/screens/home.dart';
import 'package:flum_chat/ui/screens/login.dart';
import 'package:flum_chat/ui/screens/settings.dart';
import 'package:flum_chat/ui/screens/splash.dart';
import 'package:get/get.dart';

import 'bindings/chat_binding.dart';
import 'bindings/home_binding.dart';

class AppRoutes {
  AppRoutes._();

  static final routes = [
    GetPage(
      name: '/',
      page: () => SplashScreen(),
    ),
    GetPage(
      name: '/login',
      page: () => LoginScreen(),
    ),
    GetPage(
        name: '/home',
        page: () => HomeScreen(),
        binding: HomeBinding(),
        children: [
          GetPage(
              name: '/settings',
              page: () => SettingScreen(),
              binding: SettingBinding()),
          GetPage(
              name: '/chats', page: () => ChatScreen(), binding: ChatBinding()),
        ])
  ];
}
