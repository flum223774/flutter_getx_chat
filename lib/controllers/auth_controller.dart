import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AuthController extends GetxController {
  static AuthController to = Get.find();
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  late SharedPreferences prefs;

  RxBool _isLoading = false.obs;
  RxBool _isLoggedIn = false.obs;
  Rx<User?> _currentUser = Rx<User?>(null);
  Rx<String?> _currentUserId = Rx<String?>(null);

  String? get currentUserId => _currentUserId.value;

  User? get currentUser => _currentUser.value;

  bool get isLoading => _isLoading.value;

  set isLoading(bool load) => _isLoading.value = load;

  bool get isLoggedIn => _isLoggedIn.value;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> isSignedIn() async {
    _isLoading(true);
    prefs = await SharedPreferences.getInstance();
    print('zzzz ${prefs.getString('id')}');
    _isLoggedIn(await googleSignIn.isSignedIn());
    // if (isLoggedIn) {
    //   Get.toNamed('/home', arguments: {"currentUserId": prefs.getString('id')});
    // }
    if (isLoggedIn) {
      _currentUserId.value = prefs.getString('id');
    }
    _isLoading(false);
  }

  Future<Null> handleSignIn() async {
    prefs = await SharedPreferences.getInstance();

    _isLoading(true);
    print('Start sign');
    GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    print('googleUser $googleUser');
    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      User? firebaseUser =
          (await firebaseAuth.signInWithCredential(credential)).user;
      if (firebaseUser != null) {
        // Check is already sign up
        final QuerySnapshot result = await FirebaseFirestore.instance
            .collection('users')
            .where('id', isEqualTo: firebaseUser.uid)
            .get();
        final List<DocumentSnapshot> documents = result.docs;
        if (documents.length == 0) {
          // Update data to server if new user
          FirebaseFirestore.instance
              .collection('users')
              .doc(firebaseUser.uid)
              .set({
            'nickname': firebaseUser.displayName,
            'photoUrl': firebaseUser.photoURL,
            'id': firebaseUser.uid,
            'createdAt': DateTime.now().millisecondsSinceEpoch.toString(),
            'chattingWith': null
          });

          // Write data to local
          _currentUser(firebaseUser);
          await prefs.setString('id', currentUser!.uid);
          await prefs.setString('nickname', currentUser!.displayName!);
          await prefs.setString('photoUrl', currentUser!.photoURL!);
        } else {
          // Write data to local
          await prefs.setString('id', documents[0].get('id'));
          await prefs.setString('nickname', documents[0].get('nickname'));
          await prefs.setString('photoUrl', documents[0].get('photoUrl'));
          await prefs.setString('aboutMe', documents[0].get('aboutMe'));
        }
        Fluttertoast.showToast(msg: "Sign in success");
        // Get.toNamed('/home', arguments: {'currentUserId': firebaseUser.uid});
      } else {
        Fluttertoast.showToast(msg: "Sign in fail");
      }
    }
    _isLoading(false);
  }

  Future<Null> handleSignOut() async {
    prefs = await SharedPreferences.getInstance();
    _isLoading(true);
    await firebaseAuth.signOut();
    await googleSignIn.disconnect();
    await googleSignIn.signOut();
    _isLoggedIn(false);
    _isLoading(false);
    Get.offAllNamed('/');
  }
}
