import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingController extends GetxController {
  late TextEditingController controllerNickname = TextEditingController();
  late TextEditingController controllerAboutMe = TextEditingController();

  late SharedPreferences prefs;

  String id = '';
  String nickname = '';
  String aboutMe = '';
  String photoUrl = '';

  bool isLoading = false;
  File? avatarImageFile;

  final FocusNode focusNodeNickname = FocusNode();
  final FocusNode focusNodeAboutMe = FocusNode();

  @override
  void onInit() {
    super.onInit();
    readLocal();
  }

  void readLocal() async {
    prefs = await SharedPreferences.getInstance();
    id = prefs.getString('id') ?? '';
    nickname = prefs.getString('nickname') ?? '';
    aboutMe = prefs.getString('aboutMe') ?? '';
    photoUrl = prefs.getString('photoUrl') ?? '';

    controllerNickname = TextEditingController(text: nickname);
    controllerAboutMe = TextEditingController(text: aboutMe);

    // Force refresh input
    // setState(() {});
  }

  Future getImage() async {
    ImagePicker imagePicker = ImagePicker();
    PickedFile? pickedFile;
    File? image;

    pickedFile = await imagePicker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      image = File(pickedFile.path);
    }

    if (image != null) {
      avatarImageFile = image;
      isLoading = true;
    }
    uploadFile();
  }

  Future uploadFile() async {
    String fileName = id;
    Reference reference = FirebaseStorage.instance.ref().child(fileName);
    UploadTask uploadTask = reference.putFile(avatarImageFile!);
    TaskSnapshot storageTaskSnapshot;
    uploadTask.then((value) {
      storageTaskSnapshot = value;
      storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
        photoUrl = downloadUrl;
        FirebaseFirestore.instance.collection('users').doc(id).update({
          'nickname': nickname,
          'aboutMe': aboutMe,
          'photoUrl': photoUrl
        }).then((data) async {
          await prefs.setString('photoUrl', photoUrl);
          isLoading = false;
          Fluttertoast.showToast(msg: "Upload success");
        }).catchError((err) {
          isLoading = false;
          Fluttertoast.showToast(msg: err.toString());
        });
      }, onError: (err) {
        isLoading = false;
        Fluttertoast.showToast(msg: 'This file is not an image');
      });
    }, onError: (err) {
      isLoading = false;
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  void handleUpdateData() {
    focusNodeNickname.unfocus();
    focusNodeAboutMe.unfocus();

    isLoading = true;

    FirebaseFirestore.instance.collection('users').doc(id).update({
      'nickname': nickname,
      'aboutMe': aboutMe,
      'photoUrl': photoUrl
    }).then((data) async {
      await prefs.setString('nickname', nickname);
      await prefs.setString('aboutMe', aboutMe);
      await prefs.setString('photoUrl', photoUrl);

      isLoading = false;

      Fluttertoast.showToast(msg: "Update success");
    }).catchError((err) {
      isLoading = false;

      Fluttertoast.showToast(msg: err.toString());
    });
  }
}
