import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flum_chat/controllers/auth_controller.dart';
import 'package:flum_chat/models/choice.dart';
import 'package:flum_chat/models/message.dart';
import 'package:flum_chat/ui/widgets/message_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class HomeController extends GetxController {
  AuthController authController = AuthController.to;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final ScrollController listScrollController = ScrollController();

  int _limit = 20;

  int get limit => _limit;
  int _limitIncrement = 20;

  int get limitIncrement => _limitIncrement;
  bool isLoading = false;
  List<Choice> choices = const <Choice>[
    const Choice(title: 'Settings', icon: Icons.settings),
    const Choice(title: 'Log out', icon: Icons.exit_to_app),
  ];

  static const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
  );

  @override
  void onInit() {
    super.onInit();
    registerNotification();
    configLocalNotification();
    listScrollController.addListener(scrollListener);
  }

  Future<void> registerNotification() async {
    await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    //
    // firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
    //   print('onMessage: $message');
    //   Platform.isAndroid
    //       ? showNotification(message['notification'])
    //       : showNotification(message['aps']['alert']);
    //   return;
    // }, onResume: (Map<String, dynamic> message) {
    //   print('onResume: $message');
    //   return;
    // }, onLaunch: (Map<String, dynamic> message) {
    //   print('onLaunch: $message');
    //   return;
    // });
    firebaseMessaging.getInitialMessage().then((message) {
      if (message != null) {
        Get.toNamed('/message', arguments: MessageArguments(message, true));
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
              ),
            ));
      }
    });

    firebaseMessaging.getToken().then((token) {
      print('token: $token, ${authController.currentUserId}');
      FirebaseFirestore.instance
          .collection('users')
          .doc(authController.currentUserId)
          .update({'pushToken': token});
    }).catchError((err) {
      print(err.message.toString());
      Fluttertoast.showToast(msg: err.message.toString());
    });
  }

  void configLocalNotification() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  void scrollListener() {
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange) {
      _limit += _limitIncrement;
    }
  }

  void onItemMenuPress(Choice choice) {
    if (choice.title == 'Log out') {
      AuthController.to.handleSignOut();
    } else {
      Get.toNamed('/home/settings');
    }
  }

  void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'net.gonex.flumChat',
      'Flutter chat demo',
      'your channel description',
      playSound: true,
      enableVibration: true,
      importance: Importance.max,
      priority: Priority.high,
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    print(message);
//    print(message['body'].toString());
//    print(json.encode(message));

    await flutterLocalNotificationsPlugin.show(0, message['title'].toString(),
        message['body'].toString(), platformChannelSpecifics,
        payload: json.encode(message));

//    await flutterLocalNotificationsPlugin.show(
//        0, 'plain title', 'plain body', platformChannelSpecifics,
//        payload: 'item x');
  }

  Future<bool> onBackPress() {
    openDialog();
    return Future.value(false);
  }

  Future<Null> openDialog() async {
    switch (await Get.dialog(MessageAlert())) {
      case 0:
        break;
      case 1:
        exit(0);
        break;
    }
  }
}
