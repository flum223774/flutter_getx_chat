import 'package:flum_chat/controllers/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:flum_chat/configs/const.dart';
import 'package:flum_chat/ui/widgets/loading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class LoginScreen extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Login',
            style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[
            Center(
              child: ElevatedButton(
                onPressed: () => controller.handleSignIn().then((value) {
                  Get.offAllNamed('/home');
                }).catchError((err) {
                  print(err);
                  Fluttertoast.showToast(msg: "Sign in fail");
                  controller.isLoading = false;
                }),
                child: Text(
                  'SIGN IN WITH GOOGLE',
                  style: TextStyle(fontSize: 16.0),
                ),
                style: ElevatedButton.styleFrom(
                    // color: Color(0xffdd4b39),
                    // highlightColor: Color(0xffff7f7f),
                    // splashColor: Colors.transparent,
                    // textColor: Colors.white,
                    padding: EdgeInsets.fromLTRB(30.0, 15.0, 30.0, 15.0)),
              ),
            ),

            // Loading
            Positioned(
              child: controller.isLoading ? const Loading() : Container(),
            ),
          ],
        ));
  }
}
