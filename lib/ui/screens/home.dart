import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flum_chat/controllers/auth_controller.dart';
import 'package:flum_chat/controllers/home_controller.dart';
import 'package:flum_chat/models/UserModel.dart';
import 'package:flum_chat/models/choice.dart';
import 'package:flum_chat/ui/screens/chat.dart';
import 'package:flutter/material.dart';
import 'package:flum_chat/configs/const.dart';
import 'package:flum_chat/ui/widgets/loading.dart';
import 'package:get/get.dart';

class HomeScreen extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'MAIN',
          style: TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<Choice>(
            onSelected: controller.onItemMenuPress,
            itemBuilder: (BuildContext context) {
              return controller.choices.map((Choice choice) {
                return PopupMenuItem<Choice>(
                    value: choice,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          choice.icon,
                          color: primaryColor,
                        ),
                        Container(
                          width: 10.0,
                        ),
                        Text(
                          choice.title,
                          style: TextStyle(color: primaryColor),
                        ),
                      ],
                    ));
              }).toList();
            },
          ),
        ],
      ),
      body: WillPopScope(
        child: Stack(
          children: <Widget>[
            // List
            Container(
              child: StreamBuilder<QuerySnapshot<UserModel>>(
                stream: FirebaseFirestore.instance
                    .collection('users')
                    .withConverter<UserModel>(
                        fromFirestore: (snapshots, _) =>
                            UserModel.fromJson(snapshots.data()!),
                        toFirestore: (user, _) => user.toJson())
                    .limit(controller.limit)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                      ),
                    );
                  } else {
                    final data = snapshot.requireData;
                    return ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemBuilder: (context, index) =>
                          buildItem(context, snapshot.data!.docs[index].data()),
                      itemCount: data.size,
                      controller: controller.listScrollController,
                    );
                  }
                },
              ),
            ),

            // Loading
            Positioned(
              child: controller.isLoading ? const Loading() : Container(),
            )
          ],
        ),
        onWillPop: controller.onBackPress,
      ),
    );
  }

  Widget buildItem(BuildContext context, UserModel userModel) {
    if (userModel.id == AuthController.to.currentUserId) {
      return Container();
    } else {
      return Container(
        child: ElevatedButton(
          child: Row(
            children: <Widget>[
              Material(
                child: userModel.photoUrl != null
                    ? CachedNetworkImage(
                        placeholder: (context, url) => Container(
                          child: CircularProgressIndicator(
                            strokeWidth: 1.0,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(themeColor),
                          ),
                          width: 50.0,
                          height: 50.0,
                          padding: EdgeInsets.all(15.0),
                        ),
                        imageUrl: userModel.photoUrl!,
                        width: 50.0,
                        height: 50.0,
                        fit: BoxFit.cover,
                      )
                    : Icon(
                        Icons.account_circle,
                        size: 50.0,
                        color: greyColor,
                      ),
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
                clipBehavior: Clip.hardEdge,
              ),
              Flexible(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Nickname: ${userModel.nickname}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                      ),
                      Container(
                        child: Text(
                          'About me: ${userModel.aboutMe ?? 'Not available'}',
                          style: TextStyle(color: primaryColor),
                        ),
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(left: 20.0),
                ),
              ),
            ],
          ),
          onPressed: () {
            Get.toNamed('/home/chats', arguments: {
              "peerId": userModel.id,
              'peerAvatar': userModel.photoUrl,
            });
          },
          style: ElevatedButton.styleFrom(
            primary: greyColor2,
            padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
          ),
        ),
        margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
      );
    }
  }
}
