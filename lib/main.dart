import 'package:firebase_core/firebase_core.dart';
import 'package:flum_chat/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'bindings/app_bindding.dart';
import 'configs/const.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Chat Demo',
      theme: ThemeData(
        primaryColor: themeColor,
      ),
      debugShowCheckedModeBanner: false,
      getPages: AppRoutes.routes,
      initialRoute: '/',
      initialBinding: AppBinding(),
    );
  }
}
